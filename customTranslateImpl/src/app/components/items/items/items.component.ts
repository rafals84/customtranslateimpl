import { Component, OnInit } from '@angular/core';
import { CustromTranslateService } from 'src/app/services/custrom-translate.service';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent {

  serviceSendWord: string;
  constructor(private custromTranslateService: CustromTranslateService) { 
    
  }
  changeLanguage(language: string){
    this.custromTranslateService.getTranslateFile(language);
  }

  useTransService(){
    this.serviceSendWord = this.custromTranslateService.translate('mypage.title',{"title": "My Titleeee"});
  }

}
