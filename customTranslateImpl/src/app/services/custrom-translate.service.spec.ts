import { TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CustromTranslateService } from './custrom-translate.service';

describe('CustromTranslateService', () => {
  let service: CustromTranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustromTranslateService]

    });
    service = TestBed.inject(CustromTranslateService);
    service.getTranslateFile('en');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
