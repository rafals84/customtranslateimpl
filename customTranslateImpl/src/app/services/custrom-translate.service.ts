import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class CustromTranslateService {
  
  translateJson: unknown;
  
  constructor(private http: HttpClient) {}

  getTranslateFile(language: string) {
    this.http.get(`../../assets/${language}.json`).subscribe((json) => {
      this.translateJson = json;
    });
  }

  translate(value: string, pipeArgument: unknown = {}) {
    const valuesArray = value.split(".");
    let searchObject;
    for (let index = 0; index < valuesArray.length; index++) {
      if (index === 0) {
        searchObject = this.getJsonScrap(valuesArray[index], this.translateJson);
      } else {
        searchObject = this.getJsonScrap(valuesArray[index], searchObject);
      }
    }
    const isArguments = Object.keys(pipeArgument).length === 0;
    if(searchObject !== undefined){
      searchObject = isArguments ? searchObject : this.getParamString(searchObject, pipeArgument);
    }
    return searchObject;
  }

  private getJsonScrap(value: string, givenJson) {
    let phrase;
    for (const key in givenJson) {
      if (key === value) {
        phrase = givenJson[key];
      }
    }
    return phrase;
  }

  private getParamString(searchObject: string, pipeArgument) {
    const firstIndexOfParam = searchObject.indexOf('{{');
    const lastIndexOfParam = searchObject.lastIndexOf('}}');
    const paramValue = searchObject.substring(
      firstIndexOfParam + 2,
      lastIndexOfParam
    );
    const pipeArgumentKey = Object.keys(pipeArgument)[0];
    if (pipeArgumentKey === paramValue) {
      return searchObject.replace(`{{${paramValue}}}`, pipeArgument[pipeArgumentKey]);
    }
    return searchObject;
  }
}
