import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ItemsComponent } from './components/items/items/items.component';
import { CustomTranslatePipe } from './pipes/custom-translate.pipe';
import { CustromTranslateService } from './services/custrom-translate.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    CustomTranslatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [CustromTranslateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
