import { Pipe, PipeTransform } from '@angular/core';
import { CustromTranslateService } from '../services/custrom-translate.service';

@Pipe({
  name: 'customTranslate',
  pure: false
})
export class CustomTranslatePipe implements PipeTransform {
  constructor(private custromTranslateService: CustromTranslateService) {}

  transform(value: string, ...args: any[]): any {
    let argumentJson;
    if (args.length > 0) {
      argumentJson = JSON.parse(args[0]);
    }
    const  searchedPhrase = this.custromTranslateService.translate(value, argumentJson);
    return searchedPhrase;
  }

}
