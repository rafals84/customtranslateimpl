import { Component } from '@angular/core';
import { CustromTranslateService } from './services/custrom-translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private custromTranslateService: CustromTranslateService){
    custromTranslateService.getTranslateFile('pl');
  }
  
}
